
### Running application

- `gradlew bootRun`
- Start the application at http://localhost:8080


### Test users

| Username | Password |
|----------|----------|
| user1    | password |
| user2    | pass2    |
| user3    | pass3    |
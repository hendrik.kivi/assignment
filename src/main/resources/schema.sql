CREATE TABLE user (
  id int NOT NULL AUTO_INCREMENT,
  username varchar(64) NOT NULL,
  password varchar(64) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE country (
  id int NOT NULL AUTO_INCREMENT,
  name varchar(128) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE client (
  id int NOT NULL AUTO_INCREMENT,
  first_name varchar(128) NOT NULL,
  last_name varchar(128) NOT NULL,
  username varchar(128) NOT NULL,
  email varchar(128),
  address varchar(128) NOT NULL,
  user_id int NOT NULL,
  country_id int NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES user (id),
  FOREIGN KEY (country_id) REFERENCES country (id)
);
INSERT INTO user (username, password) VALUES
	('user1', '$2a$12$wuqgpn5Z7/E1NMcED.qg8.NP74F5pfhrUGqJFuKFlUnjZFjENk62C'), --password
	('user2', '$2a$12$6FpWBaChXZQIS0cjORZHnutpJ4kPkVtx4BYK5FIjMvaG/bJ76B4wq'), --pass2
	('user3', '$2a$12$9SXWMRVDHnxub/cjHQ2kxup6YLgcqi6DxLIJat1l/3erZSGCQwE9y'); --pass3
	
INSERT INTO country (name) VALUES
	('Estonia'),
	('Latvia'),
	('Lithuania'),
	('Finland'),
	('Sweden');

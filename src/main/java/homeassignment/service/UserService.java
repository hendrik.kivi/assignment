package homeassignment.service;

import homeassignment.model.User;

public interface UserService {
	public User findByUsername(String username);
}

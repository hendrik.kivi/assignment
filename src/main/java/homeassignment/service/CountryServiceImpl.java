package homeassignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import homeassignment.model.Country;
import homeassignment.repository.CountryRepository;

@Service
public class CountryServiceImpl implements CountryService {

	@Autowired
	CountryRepository countryRepo;
	
	@Override
	public List<Country> getCountries() {
		return countryRepo.findAll();
	}
	
}

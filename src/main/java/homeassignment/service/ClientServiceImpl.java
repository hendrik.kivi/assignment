package homeassignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import homeassignment.model.Client;
import homeassignment.model.User;
import homeassignment.repository.ClientRepository;

@Service
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientRepository clientRepo;
	
	@Override
	public List<Client> getAllUserClients(User user) {
		return clientRepo.findByUserId(user.getId());
	}

	@Override
	public Client saveClient(Client client) {
		return clientRepo.save(client);
	}

	@Override
	public void updateClient(Client client) {
		Client dbClient = clientRepo.getById(client.getId());
		dbClient.setFirstName(client.getFirstName());
		dbClient.setLastName(client.getLastName());
		dbClient.setUsername(client.getUsername());
		dbClient.setEmail(client.getEmail());
		dbClient.setAddress(client.getAddress());
		dbClient.setCountry(client.getCountry());
		clientRepo.save(dbClient);
	}

	@Override
	public Client getClientById(Long id) {
		return clientRepo.getById(id);
	}

}

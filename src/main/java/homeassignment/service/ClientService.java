package homeassignment.service;

import java.util.List;

import homeassignment.model.Client;
import homeassignment.model.User;

public interface ClientService {
	List<Client> getAllUserClients(User user);
	
	Client saveClient(Client client);
	
	void updateClient(Client client);
	
	Client getClientById(Long id);
}

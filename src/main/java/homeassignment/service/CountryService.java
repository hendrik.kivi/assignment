package homeassignment.service;

import java.util.List;

import homeassignment.model.Country;

public interface CountryService {
	public List<Country> getCountries();
}

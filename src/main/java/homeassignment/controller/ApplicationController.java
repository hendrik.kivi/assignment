package homeassignment.controller;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import homeassignment.model.Client;
import homeassignment.model.Country;
import homeassignment.model.User;
import homeassignment.service.ClientService;
import homeassignment.service.CountryService;
import homeassignment.service.UserService;

@Controller
public class ApplicationController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private ClientService clientService;
	
	@Autowired
	private CountryService countryService;
	
	@GetMapping("")
	public String viewHome() {
		return "redirect:viewClients";
	}
	
	@GetMapping("viewClients")
    public String viewClients(Principal principal, Model model) {
		User currentUser = userService.findByUsername(principal.getName());
        List<Client> clients = clientService.getAllUserClients(currentUser);
        model.addAttribute("clients", clients);
		return "index";
    }
	
	@GetMapping("/addClient")
    public String viewAddClient(Model model) {
		model.addAttribute("client", new Client());
		List<Country> countries = countryService.getCountries();
		model.addAttribute("countries", countries);
		return "add-client";
    }
	
	@PostMapping("/saveClient")
    public String saveClient(@Valid @ModelAttribute Client client, BindingResult bindingResult, Principal principal, Model model) {
		if (bindingResult.hasErrors()) {
			List<Country> countries = countryService.getCountries();
			model.addAttribute("countries", countries);
			return "add-client";
		}
		client.setUser(userService.findByUsername(principal.getName()));
		clientService.saveClient(client);
        return "redirect:viewClients";
    }
	
	@GetMapping("/editClient")
    public String viewEditClient(Model model, @RequestParam Long id) {
		Client client = clientService.getClientById(id);
		model.addAttribute("client", client);
		List<Country> countries = countryService.getCountries();
		model.addAttribute("countries", countries);
		return "edit-client";
    }
	
	@PostMapping("/updateClient")
    public String updateClient(@Valid @ModelAttribute Client client, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			List<Country> countries = countryService.getCountries();
			model.addAttribute("countries", countries);
			return "edit-client";
		}
		clientService.updateClient(client);
        return "redirect:viewClients";
    }
	
}

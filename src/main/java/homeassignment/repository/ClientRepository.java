package homeassignment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import homeassignment.model.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {
	List<Client> findByUserId(long userId);
}

package homeassignment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import homeassignment.model.User;

public interface UserRepository extends JpaRepository<User, Long>  {
	public User findByUsername(String username);
}

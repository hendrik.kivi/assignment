package homeassignment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import homeassignment.model.Country;

public interface CountryRepository extends JpaRepository<Country, Long> {

}
